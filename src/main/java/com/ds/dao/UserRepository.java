package com.ds.dao;

import java.util.Collection;

import com.ds.dto.User;

public interface UserRepository {

	public Collection<User> getUserFromDB1();
	public Collection<User> getUserFromDB2();
	
}
