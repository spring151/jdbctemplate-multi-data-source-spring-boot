package com.ds.dao;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ds.dto.User;
import com.ds.mapper.UserMapper;

@Service
public class UserRepositoryImpl implements UserRepository{

	@Autowired
	@Qualifier("primaryJdbcTemplate")
	private JdbcTemplate primaryJdbcTemplate;

	@Autowired
	@Qualifier("secondaryJdbcTemplate")
	private JdbcTemplate secondaryJdbcTemplate;
	
	@Override
	public Collection<User> getUserFromDB1() {
		 String sql = "SELECT * FROM test_table1";
		    return primaryJdbcTemplate.query(sql, new UserMapper());
	}

	@Override
	public Collection<User> getUserFromDB2() {
		 String sql = "SELECT * FROM test_table2";
		    return secondaryJdbcTemplate.query(sql, new UserMapper());
	}

}
