package com.ds.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ds.dto.User;

public class UserMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user=new User();
		user.setId(rs.getString("id"));
		user.setName(rs.getString("name"));
		user.setAddress(rs.getString("address"));
		return user;
	}

}
