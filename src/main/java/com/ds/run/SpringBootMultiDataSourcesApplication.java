package com.ds.run;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.ds.dto.User;
import com.ds.service.UserService;

@SpringBootApplication
@ComponentScan(basePackages = "com.ds.*")
public class SpringBootMultiDataSourcesApplication implements CommandLineRunner{

	@Autowired
	UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootMultiDataSourcesApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		for (User user: userService.getUsers()) {
			System.out.println(user.getId()+"|"+user.getName()+"|"+user.getAddress());
		};
	}

}
