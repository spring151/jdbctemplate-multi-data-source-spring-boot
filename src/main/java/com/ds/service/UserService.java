package com.ds.service;

import java.util.Collection;
import com.ds.dto.User;

public interface UserService {

	public Collection<User> getUsers();
}
