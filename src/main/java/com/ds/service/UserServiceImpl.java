package com.ds.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ds.dao.UserRepository;
import com.ds.dto.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	@Override
	public Collection<User> getUsers() {
		Collection<User> allUSers=new ArrayList<User>();
		allUSers.addAll(userRepository.getUserFromDB1());
		allUSers.addAll(userRepository.getUserFromDB2());
		return allUSers;
	}

}
